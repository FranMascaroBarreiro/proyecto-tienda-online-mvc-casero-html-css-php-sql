<?php

	class Utils {
		
		/*
			deleteSession
			1. Elimina la variable de Sesión deseada
		*/
		public static function deleteSession($name) {
			if(isset($_SESSION[$name])){
				$_SESSION[$name] = NULL;
				unset($_SESSION[$name]);
			}
			return $name;
		}

		/*
			isAdmin:
			1. Si el Usuario loggeado no es Admin no podrá acceder a un Recurso concreto
				· En caso de intentar forzar la URL en el navegador, será redirigido a la Página Principal
			2. En caso contrario, podrá acecder al Recurso solicitado
		*/
		public static function isAdmin(){
			if(!isset($_SESSION['admin'])){
				header("Location:".base_url);
			} else {
				return true;
			}
		}

		/*
			showCategorias:
			1. Muestra las Categorías en el Menú y al Crear un nuevo Producto
		*/
		public static function showCategorias(){
			require_once 'models/categoria.php';
			$categoria = new Categoria();
			$categorias = $categoria->getAll();
			return $categorias;
		}

		/*
			statsCarrito:
			1. Encapsulamos en una Variable las Estadísticas del Carrito
			2. Recorremos el Carrito para calcular el Total del Pedido
			3. Devolvemos las Estadísticas
		*/
		public static function statsCarrito(){
			# 1
			$stats = array(
				'count' => 0,
				'total' => 0
			);
			# 2
			if(isset($_SESSION["carrito"])){
				foreach($_SESSION["carrito"] as $index => $producto){
					$stats["count"] += $producto["unidades"];
					$stats["total"] += $producto["precio"] * $producto["unidades"];
				}
			}
			# 3
			return $stats;
		}

		/*
			showStatus:
			1. Muestra en Castellano los idiomas que en Base de Datos se guardan en Inglés
		*/
		public static function showStatus($status){
			$value = "Pendiente";
			switch($status){
				case "Confirm": $value = "Pendiente"; break;
				case "Processing": $value = "Procesando"; break;
				case "Ready": $value = "Preparado para Enviar"; break;
				case "Sended": $value = "Enviado"; break;
				default: $value = "Pendiente";
			}
			return $value;
		}

	}

?>