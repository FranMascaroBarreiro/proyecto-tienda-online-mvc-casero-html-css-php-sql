<section>
	<?php if(isset($_SESSION["carrito"]) && count($_SESSION["carrito"]) >= 1): ?>
	<h3>Carrito de la compra</h3>
	<table class="table">
		<thead>
			<tr>
				<th>Imagen</th>
				<th>Nombre</th>
				<th>Precio</th>
				<th>Unidades</th>
				<th>Eliminar</th>
			</tr>
		</thead>
		<tbody>
			<?php
				foreach($carrito as $indice => $elemento):
				$producto = $elemento["producto"];
			?>
			<tr>
				<td class="imagen"><img src="<?=base_url?>assets/img/<?=$producto["imagen"]?>" alt=""></td>
				<td><a href="<?=base_url?>producto/ver&id=<?=$producto["id"]?>"><?=$producto["nombre"]?></a></td>
				<td><?=$producto["precio"]?>&nbsp;&euro;</td>
				<td>
					<div class="quantity">
						<a href="<?=base_url?>carrito/down&index=<?=$indice?>">-</a>
						<?=$elemento["unidades"]?>
						<a href="<?=base_url?>carrito/up&index=<?=$indice?>">+</a>
					</div>
				</td>
				<td><a href="<?=base_url?>carrito/delete&index=<?=$indice?>"><i class="fa-solid fa-trash"></i>&nbsp;&nbsp;&nbsp;Quitar</a></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php $stats = Utils::statsCarrito(); ?>
	<div class="order">
		<p>Precio total <strong><?=$stats["total"]?>&nbsp;&euro;</strong></p>
		<a href="<?=base_url?>carrito/delete_all"><i class="fa-solid fa-trash"></i>&nbsp;&nbsp;&nbsp;Vaciar carrito</a>
		<a href="<?=base_url?>pedido/hacer"><i class="fa-solid fa-circle-dollar-to-slot"></i>&nbsp;&nbsp;&nbsp;Realizar pedido</a>
	</div>
	<?php else: ?>
		<div class="empty">
			<i class="fa-solid fa-cart-shopping"></i>
			<h3>El carrito está vacío</h3>
		</div>
	<?php endif; ?>
</section>