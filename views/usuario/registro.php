<?php if (isset($_SESSION['register']) && $_SESSION['register'] == 'complete'): ?>
	<div id="alert"><p><span class="success">&#10004;</span>&nbsp;&nbsp;Registro completado</p></div>
<?php elseif (isset($_SESSION['register']) && $_SESSION['register'] == 'failed'): ?>
	<div id="alert"><p><span class="error">&#10008;</span>&nbsp;&nbsp;Registro fallido, comprueba los datos introducidos</p></div>
<?php endif; ?>
<?php Utils::deleteSession('register'); ?>
<div id="registro">
	<h3>Registrarse</h3>
	<form action="<?=base_url?>usuario/save" method="POST">
		<label for="nombre">Nombre</label>
		<input type="text" name="nombre" required><br>
		<label for="apellidos">Apellidos</label>
		<input type="text" name="apellidos" required><br>
		<label for="email">Email</label>
		<input type="email" name="email" required><br>
		<label for="password">Contraseña</label>
		<input type="password" name="password" required><br>
		<input type="submit" value="Registrarse">
	</form>
</div>