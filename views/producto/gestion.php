<?php if (isset($_SESSION['producto']) && $_SESSION['producto'] == 'complete'): ?>
	<div id="alert"><p><span class="success">&#10004;</span>&nbsp;&nbsp;Producto creado</p></div>
<?php elseif (isset($_SESSION['producto']) && $_SESSION['producto'] == 'failed'): ?>
	<div id="alert"><p><span class="error">&#10008;</span>&nbsp;&nbsp;Producto no creado, comprueba los datos introducidos</p></div>
<?php endif; ?>
<?php if (isset($_SESSION['delete']) && $_SESSION['delete'] == 'complete'): ?>
	<div id="alert"><p><span class="success">&#10004;</span>&nbsp;&nbsp;Producto eliminado</p></div>
<?php elseif (isset($_SESSION['delete']) && $_SESSION['delete'] == 'failed'): ?>
	<div id="alert"><p><span class="error">&#10008;</span>&nbsp;&nbsp;Producto no eliminado, comprueba los datos introducidos</p></div>
<?php endif; ?>
<?php Utils::deleteSession('producto'); ?>
<section>
	<h3>Gestión de Productos</h3>
	<br>
	<a href="<?=base_url?>producto/crear">Crear Producto</a>
	<br><br><br>
	<?php foreach($productos as $producto): ?>
		<article>
			<img src="<?=base_url?>assets/img/<?=$producto["imagen"]?>" alt="imagen de producto">
			<div class="info">
				<h3><?=$producto["nombre"]?></h3>
				<p><?=$producto["descripcion"]?></p>
				<p>PVP <strong><?=$producto["precio"]?>€</strong></p>
				<br>
				<div class="crud">
					<a href="<?=base_url?>producto/editar&id=<?=$producto['id']?>">Editar</a>
					<a href="<?=base_url?>producto/eliminar&id=<?=$producto['id']?>">Eliminar</a>
				</div>
				<a href="#">Comprar</a>
			</div>
		</article>
	<?php endforeach; ?>
</section>