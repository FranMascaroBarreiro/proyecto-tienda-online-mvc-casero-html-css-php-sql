<section>
	<?php if(isset($producto)): ?>
		<article class="info">
			<img src="<?=base_url?>assets/img/<?=$producto['imagen']?>" alt="<?=$producto['nombre']?>">	
			<div class="info">
				<h3><?=$producto['nombre']?></h3>
				<p><?=$producto['descripcion']?></p>
				<p>PVP <strong><?=$producto['precio']?>&nbsp;€</strong></p>
				<br>
				<a href="<?=base_url?>carrito/add&id=<?=$producto['id']?>">Comprar</a>
			</div>
		</article>
	<?php else: ?>
		<h3>El Producto no existe</h3>
	<?php endif; ?>
</section>