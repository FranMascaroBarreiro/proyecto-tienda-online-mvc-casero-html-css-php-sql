<section>
	<h3>Productos Destacados</h3>
	<?php foreach($productos as $producto): ?>
	<article>
		<a href="<?=base_url?>producto/ver&id=<?=$producto['id']?>">
			<img src="<?=base_url?>assets/img/<?=$producto['imagen']?>" alt="<?=$producto['nombre']?>">
		</a>
		<div class="info">
			<h3><?=$producto['nombre']?></h3>
			<p><?=$producto['descripcion']?></p>
			<p>PVP <strong><?=$producto['precio']?>&nbsp;€</strong></p>
			<a href="<?=base_url?>carrito/add&id=<?=$producto['id']?>">Comprar</a>
		</div>
	</article>
	<?php endforeach; ?>
</section>