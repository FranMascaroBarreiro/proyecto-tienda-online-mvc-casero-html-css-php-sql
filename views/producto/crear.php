<div id="producto">
	<?php if(isset($editar) && isset($product)): ?>
		<h3>Editar Producto <?=$product["nombre"]?></h3>
		<?php $url_action = base_url."producto/save&id=".$product["id"]; ?>
	<?php else: ?>
		<h3>Crear Producto</h3>
		<?php $url_action = base_url."producto/save"; ?>
	<?php endif; ?>
	<form action="<?=$url_action?>" method="POST" enctype="multipart/form-data">
		<label for="categoria">Categoria</label>
		<?php $categorias = Utils::showCategorias(); ?>
		<select name="categoria">
			<?php foreach($categorias as $categoria): ?>
				<option value="<?=$categoria['id']?>" <?= isset($product) && $categoria['id'] == $product["id"] ? 'selected' : ''; ?>>
					<?=$categoria["nombre"]?>
				</option>
			<?php endforeach; ?>
		</select><br><br>
		<label for="nombre">Nombre</label>
		<input type="text" name="nombre" value="<?= isset($product) ? $product['nombre'] : '' ?>"><br>
		<label for="descripcion">Descripción</label>
		<textarea name="descripcion"><?= isset($product) ? $product['descripcion'] : '' ?></textarea><br><br>
		<label for="precio">Precio</label>
		<input type="text" name="precio" value="<?= isset($product) ? $product['precio'] : '' ?>"><br>
		<label for="stock">Stock</label>
		<input type="number" name="stock" value="<?= isset($product) ? $product['stock'] : '' ?>"><br>
		<label for="imagen">Imagen</label>
		<?php if(isset($product) && !empty($product["imagen"])): ?>
			<img src="<?=base_url?>assets/img/<?=$product['imagen']?>">
		<?php endif; ?>
		<input type="file" name="imagen"><br>
		<?php if(isset($editar) && isset($product)): ?>
			<input type="submit" value="Editar Producto">
		<?php else: ?>
			<input type="submit" value="Crear Producto">
		<?php endif; ?>
	</form>
</div>