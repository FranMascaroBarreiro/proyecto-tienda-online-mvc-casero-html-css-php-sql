<section>
	<h3>Gestionar Categorías</h3>
	<?php foreach($categorias as $key => $value): ?>
		<article>
			<div class="info">
				<p><?=$value["id"].". ".$value["nombre"]?></p>
			</div>
		</article>
	<?php endforeach; ?>
	<br>
	<a href="<?=base_url?>categoria/crear">Crear Categoría</a>
	<br><br>
</section>