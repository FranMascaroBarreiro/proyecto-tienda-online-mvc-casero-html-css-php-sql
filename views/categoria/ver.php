<section>
	<?php if(isset($categoria)): ?>
		<h3><?=$categoria["nombre"]?></h3>
		<?php if(sizeof($productos) == 0): ?>
			<h3>No hay produtos para mostrar</h3>
		<?php else: ?>
			<?php foreach($productos as $producto): ?>
				<article>
					<a href="<?=base_url?>producto/ver&id=<?=$producto['id']?>">
						<img src="<?=base_url?>assets/img/<?=$producto['imagen']?>" alt="<?=$producto['nombre']?>">
					</a>
					<div class="info">
						<h3><?=$producto['nombre']?></h3>
						<p><?=$producto['descripcion']?></p>
						<p>PVP <strong><?=$producto['precio']?>&nbsp;€</strong></p>
						<br>
						<a href="<?=base_url?>carrito/add&id=<?=$producto['id']?>">Comprar</a>
					</div>
				</article>
			<?php endforeach; ?>
		<?php endif; ?>
	<?php else: ?>
		<h3>La Categoría no existe</h3>
	<?php endif; ?>
</section>