		<footer>
			<p>&copy; Fran Mascaró Barreiro | <?=date('Y')?></p>
		</footer>
		<script src="https://kit.fontawesome.com/d05ce1e6ef.js" crossorigin="anonymous"></script>
		<script type="text/javascript" src="<?=base_url?>assets/js/script.js"></script>
	</body>
</html>