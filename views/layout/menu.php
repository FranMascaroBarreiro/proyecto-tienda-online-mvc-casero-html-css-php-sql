<?php $categorias = Utils::showCategorias(); ?>
<nav>
	<ul>
		<li><a href="<?=base_url?>">Inicio</a></li>
		<?php foreach($categorias as $categoria): ?>
			<li><a href="<?=base_url?>categoria/ver&id=<?=$categoria['id']?>"><?=$categoria["nombre"]?></a></li>
		<?php endforeach; ?>
	</ul>
</nav>