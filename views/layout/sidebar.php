<aside>
	<?php if(!isset($_SESSION['identity'])): ?>
		<h3>Login</h3>
		<?php
			if(isset($_SESSION['error_login'])){
				echo "<div id='error'><p><span class='error'><i class='fa-solid fa-circle-exclamation'></i>&nbsp;".$_SESSION['error_login']."</span></p></div>";
			}
		?>
		<form action="<?=base_url?>usuario/login" method="post">
			<label for="email">Email</label>
			<input type="email" name ="email"><br>
			<label for="password">Contraseña</label>
			<input type="password" name ="password"><br>
			<input type="submit" value="Enviar">
		</form>
	<?php else: ?>
		<h3>
			<?php
				echo "<div id='success'><p><span class='success'><i class='fa-solid fa-hand-peace'></i>&nbsp;".$_SESSION["identity"]["nombre"]."&nbsp;".$_SESSION["identity"]["apellidos"]."</span></p></div>";
			?>
		</h3>
	<?php endif; ?>
	<ul>
		<?php $stats = Utils::statsCarrito(); ?>
		<li class="shopping-cart"><strong><?=$stats["count"]?></strong>&nbsp;Productos</li>
		<li class="shopping-cart"><strong><?=$stats["total"]?>&nbsp;&euro;</strong>&nbsp;Total</li>
		<li><i class="fa-solid fa-cart-shopping"></i></i>&nbsp;<a href="<?=base_url?>carrito/index">Carrito</a></li>
		<?php if(isset($_SESSION['admin'])): ?>
			<li><i class="fa-sharp fa-solid fa-layer-group"></i>&nbsp;<a href="<?=base_url?>categoria/index">Gestionar categorías</a></li>
			<li><i class="fa-solid fa-cube"></i></i>&nbsp;<a href="<?=base_url?>producto/gestion">Gestionar productos</a></li>
			<li><i class="fa-sharp fa-solid fa-truck-fast"></i>&nbsp;<a href="<?=base_url?>pedido/gestion">Gestionar pedidos</a></li>
		<?php endif; ?>
		<?php if(isset($_SESSION['identity'])): ?>
			<li><i class="fa-solid fa-user"></i>&nbsp;<a href="<?=base_url?>pedido/mis_pedidos">Mis pedidos</a></li>
			<li><i class="fa-solid fa-circle-xmark"></i>&nbsp;<a href="<?=base_url?>usuario/logout">Cerrar Sesión</a></li>
		<?php else: ?>
			<li><i class="fa-solid fa-address-card"></i>&nbsp;<a href="<?=base_url?>usuario/registro">Registro</a></li>
		<?php endif; ?>
	</ul>
</aside>