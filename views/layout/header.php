<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Tienda Online</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" type="image/x-icon" href="<?=base_url?>assets/img/favicon.ico">
		<link rel="stylesheet" type="text/css" href="<?=base_url?>assets/css/style.css"/>
	</head>
	<body>
		<header>
			<div>
				<a href="<?=base_url?>"><img src="<?=base_url?>assets/img/logo_Tienda_Online.png" alt="logo Tienda Online"></a>
			</div>
		</header>