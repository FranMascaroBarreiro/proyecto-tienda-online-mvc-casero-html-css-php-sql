<section>
	<div id="order">
		<?php if(isset($_SESSION["identity"])): ?>
			<h3>Realizar pedido</h3>
			<form action="<?=base_url?>pedido/add" method="POST">
				<label for="provincia">Provincia</label>
				<input type="text" name="provincia" required><br>
				<label for="localidad">Localidad</label>
				<input type="text" name="localidad" required><br>
				<label for="direccion">Dirección</label>
				<input type="text" name="direccion" required><br>
				<input type="submit" value="&#10004; Confirmar Pedido">
			</form>
			<a href="<?=base_url?>carrito/index"><i class="fa-solid fa-cart-shopping"></i>&nbsp;Resumen del pedido</a>
		<?php else: ?>
			<p>Necesitas estar identificado en la web para poder realizar un pedido</p>
		<?php endif; ?>
	</div>
</section>