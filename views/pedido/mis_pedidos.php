<section>
	<?php if(count($pedidos) > 0): ?>
		<h3>Gestionar Pedidos</h3>
		<table class="table">
			<thead>
				<tr>
					<th>Nº Pedido</th>
					<th>Total</th>
					<th>Fecha</th>
					<th>Estado</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($pedidos as $pedido): ?>
				<tr>
					<td><a href="<?=base_url?>pedido/detalle&id=<?=$pedido['id']?>"><?=$pedido["id"]?></a></td>
					<td><?=$pedido["coste"]?>&nbsp;&euro;</td>
					<td><?=$pedido["fecha"]?></td>
					<td><?=Utils::showStatus($pedido["estado"])?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php else: ?>
		<div class="empty">
			<i class="fa-solid fa-user"></i>
			<h3>Todavía no has realizado ningún pedido</h3>
		</div>
	<?php endif; ?>
</section>