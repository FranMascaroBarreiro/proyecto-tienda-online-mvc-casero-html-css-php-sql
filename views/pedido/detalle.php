<div id="detalle">
	<i class="fa-solid fa-circle-info"></i>
	<h3>Detalle del Pedido</h3>
	<?php if(isset($pedido)): ?>
		<ul>
			<li>Nº Pedido: <strong><?=$pedido["id"]?></strong></li>
			<li>Estado: <strong><?=Utils::showStatus($pedido["estado"])?></strong></li>
			<li>Total a pagar: <strong><?=$pedido["coste"]?>&nbsp;&euro;</strong></li>
			<li>Dirección: <strong><?=$pedido["localidad"].", ".$pedido["direccion"]?></strong></li>
		</ul>
		<table class="table">
			<thead>
				<tr>
					<th>Nº Producto</th>
					<th>Nombre</th>
					<th>Precio</th>
					<th>Unidades</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($productos as $i => $producto): ?>
					<tr>
						<td><?=++$i?></td>
						<td><?=$producto["nombre"]?></td>
						<td><?=$producto["precio"]?></td>
						<td><?=$producto["unidades"]?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	<?php endif; ?>
	<?php if(isset($_SESSION["admin"])): ?>
		<br>
		<form action="<?=base_url?>pedido/estado" method="POST">
			<input type="hidden" value="<?=$pedido['id']?>" name="pedido_id">
			<select name="estado">
				<option value="Confirm" <?= $pedido["estado"] == "Confirm" ? 'selected': ''; ?>>Pendiente</option>
				<option value="Processing" <?= $pedido["estado"] == "Processing" ? 'selected': ''; ?>>Procesando</option>
				<option value="Ready" <?= $pedido["estado"] == "Ready" ? 'selected': ''; ?>>Preparado para Enviar</option>
				<option value="Sended" <?= $pedido["estado"] == "Sended" ? 'selected': ''; ?>>Enviado</option>
			</select>
			<input type="submit" value="Cambiar Estado">
		</form>
	<?php endif; ?>
</div>