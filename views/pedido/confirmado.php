<?php if(isset($_SESSION["pedido"]) && $_SESSION["pedido"] == 'complete'): ?>
	<div id="confirmado">
		<i class="fa-solid fa-circle-check"></i>
		<h3>Tu pedido se ha confirmado</h3>
		<p>
			Tu pedido ha sido guardado con éxito. <br>
			Una vez que realices la transferencia bancaria, será procesado y enviado
		</p>
		<?php if(isset($pedido)): ?>
			<ul>
				<li>Nº Pedido: <strong><?=$pedido["id"]?></strong></li>
				<li>Total a pagar: <strong><?=$pedido["coste"]?>&nbsp;&euro;</strong></li>
			</ul>
			<table class="table">
				<thead>
					<tr>
						<th>Nº Producto</th>
						<th>Nombre</th>
						<th>Precio</th>
						<th>Unidades</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($productos as $i => $producto): ?>
						<tr>
							<td><?=++$i?></td>
							<td><?=$producto["nombre"]?></td>
							<td><?=$producto["precio"]?></td>
							<td><?=$producto["unidades"]?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>
	</div>
<?php elseif(isset($_SESSION["pedido"]) && $_SESSION["pedido"] != 'complete'): ?>
	<div id="confirmado">
		<i class="fa-solid fa-circle-exclamation"></i>
		<h3>Tu pedido no ha podido procesarse</h3>
		<p>Ha ocurrido un error</p>
	</div>
<?php endif; ?>