<?php

	class Pedido {

		private $id;
		private $usuario_id;
		private $provincia;
		private $localidad;
		private $direccion;
		private $coste;
		private $estado;
		private $fecha;
		private $hora;
		private $db;
		private $pedido_id;

		public function __construct(){
			$this->db = Database::connect();
		}

		function getId() {
			return $this->id;
		}
	
		function getUsuario_id() {
			return $this->usuario_id;
		}
	
		function getProvincia() {
			return $this->provincia;
		}
	
		function getLocalidad() {
			return $this->localidad;
		}
	
		function getDireccion() {
			return $this->direccion;
		}
	
		function getCoste() {
			return $this->coste;
		}
	
		function getEstado() {
			return $this->estado;
		}
	
		function getFecha() {
			return $this->fecha;
		}
	
		function getHora() {
			return $this->hora;
		}

		function getPedido_id() {
			return $this->pedido_id;
		}
	
		function setId($id) {
			$this->id = $id;
		}
	
		function setUsuario_id($usuario_id) {
			$this->usuario_id = $usuario_id;
		}
	
		function setProvincia($provincia) {
			$this->provincia = $provincia;
		}
	
		function setLocalidad($localidad) {
			$this->localidad = $localidad;
		}
	
		function setDireccion($direccion) {
			$this->direccion = $direccion;
		}
	
		function setCoste($coste) {
			$this->coste = $coste;
		}
	
		function setEstado($estado) {
			$this->estado = $estado;
		}
	
		function setFecha($fecha) {
			$this->fecha = $fecha;
		}
	
		function setHora($hora) {
			$this->hora = $hora;
		}

		function setPedido_id($pedido_id) {
			$this->pedido_id = $pedido_id;
		}

		/*
			getAll: Obtiene todos los Pedidos
		*/
		public function getAll(){
			$stmt = $this->db->prepare("
				SELECT * FROM pedidos ORDER BY id DESC
			");
			$stmt->execute();
			$productos = $stmt->fetchAll();
			return $productos;
		}

		/*
			getOne: Obtiene un Pedido concreto
		*/
		public function getOne(){
			$stmt = $this->db->prepare("
				SELECT * FROM pedidos WHERE id = :id
			");
			$id = $this->getId();
			$stmt->bindParam(':id', $id);
			$stmt->execute();
			$producto = $stmt->fetchAll();
			$producto = $producto[0];
			return $producto;
		}

		/*
			getOneByUser: Obtiene el ID y Coste del último Pedido realizado por un Usuario concreto
		*/
		public function getOneByUser(){
			$stmt = $this->db->prepare("
				SELECT p.id, p.coste FROM pedidos p
				INNER JOIN lineas_pedidos lp ON lp.pedido_id = p.id
				WHERE usuario_id = :usuario_id
				ORDER BY id DESC LIMIT 1
			");
			$usuario_id = $this->getUsuario_id();
			$stmt->bindParam(':usuario_id', $usuario_id);
			$stmt->execute();
			$pedido = $stmt->fetchAll();
			$pedido = $pedido[0];
			return $pedido;
		}

		/*
			getAllByUser: Obtiene todos los Pedidos de un Usuario concreto
		*/
		public function getAllByUser(){
			$stmt = $this->db->prepare("
				SELECT * FROM pedidos p
				WHERE usuario_id = :usuario_id
				ORDER BY id DESC
			");
			$usuario_id = $this->getUsuario_id();
			$stmt->bindParam(':usuario_id', $usuario_id);
			$stmt->execute();
			$pedido = $stmt->fetchAll();
			return $pedido;
		}

		/*
			getProductosByPedido: Obtiene todos los Productos de un Pedido concreto
		*/
		public function getProductosByPedido($pedido_id){
			$stmt = $this->db->prepare("
				SELECT p.*, lp.unidades FROM productos p
				INNER JOIN lineas_pedidos lp ON p.id = lp.producto_id
				WHERE pedido_id = :pedido_id
			");
			$pedido_id = $pedido_id;
			$stmt->bindParam(':pedido_id', $pedido_id);
			$stmt->execute();
			$productos = $stmt->fetchAll();
			return $productos;
		}

		/*
			save: Guarda un nuevo Pedido
		*/
		public function save(){
			$stmt = $this->db->prepare("
				INSERT INTO pedidos
				VALUES(:id, :usuario_id, :provincia, :localidad, :direccion, :coste, :estado, :fecha, :hora)
			");
			$id = NULL;
			$usuario_id = $this->getUsuario_id();
			$provincia = $this->getProvincia();
			$localidad = $this->getLocalidad();
			$direccion = $this->getDireccion();
			$coste = $this->getCoste();
			$estado = $this->getEstado();
			$fecha = date("Y-m-d H:i:s");
			$hora = $this->getHora();
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':usuario_id', $usuario_id);
			$stmt->bindParam(':provincia', $provincia);
			$stmt->bindParam(':localidad', $localidad);
			$stmt->bindParam(':direccion', $direccion);
			$stmt->bindParam(':coste', $coste);
			$stmt->bindParam(':estado', $estado);
			$stmt->bindParam(':fecha', $fecha);
			$stmt->bindParam(':hora', $hora);
			$save = $stmt->execute();
			$result = false;
			if($save){
				$result = true;
			}
			return $result;
		}

		/*
			save_linea: Guarda la asociación entre Pedidos y Productos
		*/
		public function save_linea(){
			$pedido_id = (int) $this->db->lastInsertId();
			$this->setPedido_id($pedido_id);
			foreach($_SESSION["carrito"] as $elemento){
				$producto = $elemento["producto"];
				$stmt = $this->db->prepare("
					INSERT INTO lineas_pedidos
					VALUES(:id, :pedido_id, :producto_id, :unidades)
				");
				$id = NULL;
				$pedido_id = $this->getPedido_id();
				$producto_id = $producto["id"];
				$unidades = $elemento["unidades"];
				$stmt->bindParam(':id', $id);
				$stmt->bindParam(':pedido_id', $pedido_id);
				$stmt->bindParam(':producto_id', $producto_id);
				$stmt->bindParam(':unidades', $unidades);
				$save = $stmt->execute();
			}
			$result = false;
			if($save){
				$result = true;
			}
			return $result;
		}

		/*
			edit: Actualiza el Estado de un Pedido
		*/
		public function edit(){
			$stmt = $this->db->prepare("
				UPDATE pedidos SET estado = :estado WHERE id = :id
			");
			$id = $this->getId();
			$estado = $this->getEstado();
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':estado', $estado);
			$save = $stmt->execute();
			$result = false;
			if($save){
				$result = true;
			}
			return $result;
		}
	}

?>