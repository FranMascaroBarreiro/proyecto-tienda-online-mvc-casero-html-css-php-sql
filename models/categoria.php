<?php

	class Categoria {

		private $id;
		private $nombre;
		private $db;

		public function __construct(){
			$this->db = Database::connect();
		}

		function getId() {
			return $this->id;
		}
	
		function getNombre() {
			return $this->nombre;
		}
	
		function setId($id) {
			$this->id = $id;
		}
	
		function setNombre($nombre) {
			$this->nombre = $nombre;
		}

		/*
			getAll: Obtiene todas las Categorías
		*/
		public function getAll(){
			$stmt = $this->db->prepare("
				SELECT * FROM categorias
			");
			$stmt->execute();
			$categorias = $stmt->fetchAll();
			return $categorias;
		}

		/*
			getOne: Obtiene una Categoría concreta
		*/
		public function getOne(){
			$stmt = $this->db->prepare("
				SELECT * FROM categorias WHERE id = :id
			");
			$id = $this->getId();
			$stmt->bindParam(':id', $id);
			$stmt->execute();
			$categoria = $stmt->fetchAll();
			return $categoria;
		}

		/*
			save: Crea una Categoría nueva
		*/
		public function save(){
			$stmt = $this->db->prepare("
				INSERT INTO categorias
				VALUES(:id, :nombre)
			");
			$id = NULL;
			$nombre = $this->getNombre();
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':nombre', $nombre);
			$save = $stmt->execute();
			$result = false;
			if($save){
				$result = true;
			}
			return $result;
		}
	}

?>