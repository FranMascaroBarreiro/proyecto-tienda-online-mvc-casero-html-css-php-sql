<?php

	class Usuario {

		private $id;
		private $nombre;
		private $apellidos;
		private $email;
		private $password;
		private $rol;
		private $imagen;
		private $db;

		public function __construct(){
			$this->db = Database::connect();
		}

		public function getId() {
			return $this->id;
		}
	
		public function getNombre() {
			return $this->nombre;
		}
	
		public function getApellidos() {
			return $this->apellidos;
		}
	
		public function getEmail() {
			return $this->email;
		}
	
		public function getPassword() {
			return password_hash($this->password, PASSWORD_BCRYPT, ['cost' => 4]);
		}
	
		public function getRol() {
			return $this->rol;
		}
	
		public function getImagen() {
			return $this->imagen;
		}
	
		public function setId($id) {
			$this->id = $id;
		}
	
		public function setNombre($nombre) {
			$this->nombre = $nombre;
		}
	
		public function setApellidos($apellidos) {
			$this->apellidos = $apellidos;
		}
	
		public function setEmail($email) {
			$this->email = $email;
		}
	
		public function setPassword($password) {
			$this->password = $password;
		}
	
		public function setRol($rol) {
			$this->rol = $rol;
		}
	
		public function setImagen($imagen) {
			$this->imagen = $imagen;
		}

		/*
			save: Guarda un nuevo Usuario
		*/
		public function save(){
			$stmt = $this->db->prepare("
				INSERT INTO usuarios
				VALUES(:id, :nombre, :apellidos, :email, :password, :rol, :imagen)
			");
			$id = NULL;
			$nombre = $this->getNombre();
			$apellidos = $this->getApellidos();
			$email = $this->getEmail();
			$password = $this->getPassword();
			$rol = "user";
			$imagen = NULL;
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':nombre', $nombre);
			$stmt->bindParam(':apellidos', $apellidos);
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':password', $password);
			$stmt->bindParam(':rol', $rol);
			$stmt->bindParam(':imagen', $imagen);
			$save = $stmt->execute();
			$result = false;
			if($save){
				$result = true;
			}
			return $result;
		}

		/*
			login
			1. Obtiene las credenciales del Usuario que está intentando acceder
			2. Comprueba si existe el Usuario en Base de Datos
				· Si existe, verifica la contraseña
			3. En caso de coincidir, devuelve el Usuario solicitado
		*/
		public function login(){
			# 1
			$result = false;
			$email = $this->email;
			$password = $this->password;
			# 2
			$stmt = $this->db->prepare("
				SELECT * FROM usuarios WHERE email = :email
			");
			$stmt->bindParam(':email', $email);
			$login = $stmt->execute();
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			if ($login && $stmt->rowCount() == 1) {
				$usuario = $stmt->fetchAll();
				$verify = password_verify($password, $usuario[0]["password"]);
				if ($verify) {
					$result = $usuario;
				}
			}
			# 3
			return $result;
		}
	}

?>