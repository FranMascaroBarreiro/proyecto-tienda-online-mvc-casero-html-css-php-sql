<?php

	class Producto {

		private $id;
		private $categoria_id;
		private $nombre;
		private $descripcion;
		private $precio;
		private $stock;
		private $oferta;
		private $fecha;
		private $imagen;
		private $db;

		public function __construct(){
			$this->db = Database::connect();
		}

		function getId() {
			return $this->id;
		}
	
		function getCategoria_id() {
			return $this->categoria_id;
		}
	
		function getNombre() {
			return $this->nombre;
		}
	
		function getDescripcion() {
			return $this->descripcion;
		}
	
		function getPrecio() {
			return $this->precio;
		}
	
		function getStock() {
			return $this->stock;
		}
	
		function getOferta() {
			return $this->oferta;
		}
	
		function getFecha() {
			return $this->fecha;
		}
	
		function getImagen() {
			return $this->imagen;
		}
	
		function setId($id) {
			$this->id = $id;
		}
	
		function setCategoria_id($categoria_id) {
			$this->categoria_id = $categoria_id;
		}
	
		function setNombre($nombre) {
			$this->nombre = $nombre;
		}
	
		function setDescripcion($descripcion) {
			$this->descripcion = $descripcion;
		}
	
		function setPrecio($precio) {
			$this->precio = $precio;
		}
	
		function setStock($stock) {
			$this->stock = $stock;
		}
	
		function setOferta($oferta) {
			$this->oferta = $oferta;
		}
	
		function setFecha($fecha) {
			$this->fecha = $fecha;
		}
	
		function setImagen($imagen) {
			$this->imagen = $imagen;
		}

		/*
			getAll: Obtiene todos los Productos
		*/
		public function getAll(){
			$stmt = $this->db->prepare("
				SELECT * FROM productos ORDER BY id DESC
			");
			$stmt->execute();
			$productos = $stmt->fetchAll();
			return $productos;
		}

		/*
			getAllCategory: Obtiene todos los Productos de una Categoría específica
		*/
		public function getAllCategory(){
			$stmt = $this->db->prepare("
				SELECT p.*, c.nombre as 'catnombre'
				FROM productos p
				INNER JOIN categorias c ON c.id = p.categoria_id
				WHERE p.categoria_id = :categoria_id
				ORDER BY id DESC
			");
			$categoria_id = $this->getCategoria_id();
			$stmt->bindParam(':categoria_id', $categoria_id, PDO::PARAM_INT);
			$stmt->execute();
			$productos = $stmt->fetchAll();
			return $productos;
		}

		/*
			getOne: Obtiene un Producto mediante su ID
		*/
		public function getOne(){
			$stmt = $this->db->prepare("
				SELECT * FROM productos WHERE id = :id
			");
			$id = $this->getId();
			$stmt->bindParam(':id', $id);
			$stmt->execute();
			$producto = $stmt->fetchAll();
			$producto = $producto[0];
			return $producto;
		}

		/*
			getRandom: Obtiene una cantidad aleatoria de Productos que le indiquemos
		*/
		public function getRandom($limit){
			$stmt = $this->db->prepare("
				SELECT * FROM productos ORDER BY RAND() LIMIT $limit
			");
			$stmt->execute();
			$productos = $stmt->fetchAll();
			return $productos;
		}

		/*
			save: Guarda un nuevo Producto
		*/
		public function save(){
			$stmt = $this->db->prepare("
				INSERT INTO productos
				VALUES(:id, :categoria_id, :nombre, :descripcion, :precio, :stock, :oferta, :fecha, :imagen)
			");
			$id = NULL;
			$categoria_id = $this->getCategoria_id();
			$nombre = $this->getNombre();
			$descripcion = $this->getDescripcion();
			$precio = $this->getPrecio();
			$stock = $this->getStock();
			$oferta = NULL;
			$fecha = date("Y-m-d H:i:s");
			$imagen = $this->getImagen();
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':categoria_id', $categoria_id);
			$stmt->bindParam(':nombre', $nombre);
			$stmt->bindParam(':descripcion', $descripcion);
			$stmt->bindParam(':precio', $precio);
			$stmt->bindParam(':stock', $stock);
			$stmt->bindParam(':oferta', $oferta);
			$stmt->bindParam(':fecha', $fecha);
			$stmt->bindParam(':imagen', $imagen);
			$save = $stmt->execute();	
			$result = false;
			if($save){
				$result = true;
			}
			return $result;
		}

		/*
			edit: Edita el Producto que le indiquemos
		*/
		public function edit(){
			$stmt = $this->db->prepare("
				UPDATE productos
				SET categoria_id = :categoria_id, nombre = :nombre, descripcion = :descripcion, precio = :precio, stock = :stock
				WHERE id = :id
			");
			$id = $this->getId();
			$categoria_id = $this->getCategoria_id();
			$nombre = $this->getNombre();
			$descripcion = $this->getDescripcion();
			$precio = $this->getPrecio();
			$stock = $this->getStock();
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':categoria_id', $categoria_id);
			$stmt->bindParam(':nombre', $nombre);
			$stmt->bindParam(':descripcion', $descripcion);
			$stmt->bindParam(':precio', $precio);
			$stmt->bindParam(':stock', $stock);
			$save = $stmt->execute();
			$result = false;
			if($save){
				$result = true;
			}
			return $result;
		}

		/*
			delete: Elimina el Producto que le indiquemos
		*/
		public function delete(){
			$stmt = $this->db->prepare("
				DELETE FROM productos WHERE id = :id
			");
			$id = $this->id;
			$stmt->bindParam(':id', $id);
			$delete = $stmt->execute();
			$result = false;
			if($delete){
				$result = true;
			}
			return $result;
		}
	}

?>