<?php

	class Database {

		public static function connect(){
			try {
				$db = new PDO("mysql:host=127.0.0.1;port=8889;dbname=masterPHP_Tienda_Online", "root", "root");
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				return $db;
			} catch (PDOException $e) {
				$code = $e->getCode();
				$file = $e->getFile();
				$message = $e->getMessage();
				$line = $e->getLine();
				$traceString = $e->getTraceAsString();
				echo "<pre>";
				echo "Exception thrown in: <b>$file</b>\n\n";
				echo "on Line: <b>$line</b>\n\n";
				echo "Code: [<b>$code</b>]\n\n";
				echo "with the Message: <b>$message</b>\n\n";
				echo "Trace as string: \n\n <b>$traceString</b>";
				echo "</pre>";
			}
		}
	}

?>