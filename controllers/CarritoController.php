<?php

	require_once 'models/producto.php';

	class CarritoController {
		
		/*
			Index
			1. Encapsula los datos de Sesión asociados al Carrito en una variable que se pasa a la Vista
			2. Muestra la Vista 'Carrito'
		*/
		public function index(){
			# 1
			if(isset($_SESSION["carrito"]) && count($_SESSION["carrito"]) >= 1){
				$carrito = $_SESSION["carrito"];
			} else {
				$carrito = array();
			}
			# 2
			require_once 'views/carrito/index.php';
		}

		/*
			Add: Gestiona el añadir Productos al Carrito
			1. Semantizamos el Código
			2. Revisa si en la Sesión ya se encuentra el Producto que intentamos añadir
				· En caso positivo añadimos 1 Unidad y actualizamos la Variable Auxiliar
			3. Instanciamos el Producto en el Carrito si todavía no está
			4. Muestra la Vista 'Carrito'
		*/
		public function add(){
			# 1
			if(isset($_GET['id'])){
				$producto_id = $_GET['id'];
			} else {
				header("Location:".base_url);
			}
			# 2
			if(isset($_SESSION['carrito'])){
				$counter = 0;
				foreach($_SESSION["carrito"] as $indice => $elemento){
					if($elemento["id_producto"] == $producto_id){
						$_SESSION["carrito"][$indice]["unidades"]++;
						$counter++;
					}
				}
			}
			# 3
			if(!isset($counter) || $counter == 0){
				$producto = new Producto();
				$producto->setId($producto_id);
				$producto = $producto->getOne();
				$_SESSION['carrito'][] = array(
					"id_producto" => $producto["id"],
					"precio" => $producto["precio"],
					"unidades" => 1,
					"producto" => $producto
				);
			}
			# 4
			header("Location:".base_url."carrito/index");
		}

		/*
			Delete: Elimina 1 Producto concreto del Carrito
			1. Elimina el Producto mediante su índice
			2. Muestra la Vista 'Carrito'
		*/
		public function delete(){
			# 1
			if(isset($_GET["index"])){
				$index = $_GET["index"];
				unset($_SESSION['carrito'][$index]);
			}
			# 2
			header("Location:".base_url."carrito/index");
		}

		/*
			Delete_all: Elimina todos los Productos del Carrito y recarga la Vista 'Carrito'
		*/
		public function delete_all(){
			unset($_SESSION['carrito']);
			header("Location:".base_url."carrito/index");
		}

		/*
			Up: Suma 1 Cantidad al Total de un Producto existente en el Carrito y recarga la Vista 'Carrito'
		*/
		public function up(){
			if(isset($_GET["index"])){
				$index = $_GET["index"];
				$_SESSION['carrito'][$index]["unidades"]++;
			}
			header("Location:".base_url."carrito/index");
		}

		/*
			Down:
			1. Resta 1 Cantidad al Total de un Producto existente en el Carrito
				· Si restamos de un Total de 1 Cantidad de Producto en el Carrito, ese Producto se elimina del Carrito
			2. Recarga la Vista 'Carrito'
		*/
		public function down(){
			# 1
			if(isset($_GET["index"])){
				$index = $_GET["index"];
				$_SESSION['carrito'][$index]["unidades"]--;
				if($_SESSION['carrito'][$index]["unidades"] == 0){
					unset($_SESSION['carrito'][$index]);
				}
			}
			# 2
			header("Location:".base_url."carrito/index");
		}

	}

?>