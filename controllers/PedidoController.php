<?php

	require_once 'models/pedido.php';

	class PedidoController {
		
		/*
			Hacer: Muestra la Vista 'Pedido' (Hacer)
		*/
		public function hacer(){
			require_once 'views/pedido/hacer.php';
		}

		/*
			Add
			1. Encapsulamos en Variables la info recogida mediante Formulario del Pedido
			2. Obtenemos el Coste Total del Pedido
			3. Instanciamos un Pedido y lo guardamos en Base de Datos
			4. Muestra la Vista 'Pedido' (Confirmado)
			5. Carga el Controlador Frontal
		*/
		public function add(){			
			if(isset($_SESSION["identity"])){
				# 1
				$usuario_id = $_SESSION["identity"]["id"];
				$provincia = isset($_POST["provincia"]) ? $_POST["provincia"] : false;
				$localidad = isset($_POST["localidad"]) ? $_POST["localidad"] : false;
				$direccion = isset($_POST["direccion"]) ? $_POST["direccion"] : false;
				# 2
				$stats = Utils::statsCarrito();
				$coste = $stats["total"];
				# 3
				if($provincia && $localidad && $direccion){
					$pedido = new Pedido();
					$pedido->setUsuario_id($usuario_id);
					$pedido->setProvincia($provincia);
					$pedido->setLocalidad($localidad);
					$pedido->setDireccion($direccion);
					$pedido->setCoste($coste);
					$pedido->setEstado("Processing");

					$save = $pedido->save();
					$save_lineas = $pedido->save_linea();

					if($save && $save_lineas){
						$_SESSION["pedido"] = "complete";
					} else {
						$_SESSION["pedido"] = "failed";
					}
				} else {
					$_SESSION["pedido"] = "failed";
				}
				# 4
				header("Location: ".base_url.'pedido/confirmado');
			} else {
				# 5
				header("Location: ".base_url);
			}
		}

		/*
			Confirmado
			1. Encapsula en una Variable el ID del Usuario loggeado
			2. Obtiene el último Pedido realizado del Usuario loggeado
			3. Obtiene los Productos del Pedido seleccionado
			4. Muestra la Vista 'Pedido' (Confirmado)
		*/
		public function confirmado(){
			if(isset($_SESSION["identity"])){
				# 1
				$identity = $_SESSION["identity"];
				# 2
				$pedido = new Pedido();
				$pedido->setUsuario_id($identity["id"]);
				$pedido = $pedido->getOneByUser();
				# 3
				$pedido_productos = new Pedido();
				$productos = $pedido_productos->getProductosByPedido($pedido["id"]);
			}
			# 4
			require_once 'views/pedido/confirmado.php';
		}

		/*
			Mis_pedidos
			1. Obtiene todos los Pedidos realizados por parte del Usuario loggeado
			2. Muestra la Vista 'Pedido' (Mis Pedidos)
		*/
		public function mis_pedidos(){
			# 1
			$usuario_id = $_SESSION["identity"]["id"];
			$pedido = new Pedido();
			$pedido->setUsuario_id($usuario_id);
			$pedidos = $pedido->getAllByUser();
			# 2
			require_once 'views/pedido/mis_pedidos.php';
		}

		/*
			Detalle
			1. Encapsula en una Variable el ID del Pedido
			2. Obtiene los Detalles del Pedido seleccionado
			3. Obtiene los Productos del Pedido seleccionado
			4. Muestra la Vista 'Pedido' (Detalle)
			5. Muestra la Vista 'Pedido' (Mis Pedidos)
		*/
		public function detalle(){
			if(isset($_GET["id"])){
				# 1
				$id = $_GET["id"];
				# 2
				$pedido = new Pedido();
				$pedido->setId($id);
				$pedido = $pedido->getOne();
				# 3
				$pedido_productos = new Pedido();
				$productos = $pedido_productos->getProductosByPedido($pedido["id"]);
				# 4
				require_once 'views/pedido/detalle.php';
			} else {
				# 5
				header("Location: ".base_url.'pedido/mis_pedidos');
			}
		}

		/*
			Gestion:
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Obtiene todos los Pedidos
			3. Muestra la Vista 'Pedido' (Mis Pedidos)
		*/
		public function gestion(){
			# 1
			Utils::isAdmin();
			# 2
			$pedido = new Pedido();
			$pedidos = $pedido->getAll();
			# 3
			require_once 'views/pedido/mis_pedidos.php';
		}

		/*
			Estado
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Encapsulamos los Datos del Formulario obtenidos mediante POST
			3. Actualizamos el Estado del Pedido
			4. Muestra la Vista 'Pedido' (Detalle)
			5. Carga el Controlador Frontal
		*/
		public function estado(){
			# 1
			Utils::isAdmin();
			if(isset($_POST["pedido_id"])){
				# 2
				$id = $_POST["pedido_id"];
				$estado = $_POST["estado"];
				# 3
				$pedido = new Pedido();
				$pedido->setId($id);
				$pedido->setEstado($estado);
				$pedido->edit();
				# 4
				header("Location: ".base_url.'pedido/detalle&id='.$id);
			} else {
				# 5
				header("Location: ".base_url);
			}
		}

	}

?>