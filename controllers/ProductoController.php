<?php

	require_once 'models/producto.php';

	class ProductoController {
		
		/*
			Index:
			1. Instancia un Producto y obtiene 6 de ellos de forma Aleatoria
			2. Muestra la Vista 'Producto' (Destacados)
		*/
		public function index(){
			# 1
			$producto = new Producto();
			$productos = $producto->getRandom(6);
			# 2
			require_once 'views/producto/destacados.php';
		}

		/*
			Ver:
			1. Encapsula en una Variable el ID del Producto que obtiene mediante la URL y accede a él
			2. Muestra la Vista 'Producto' (Ver)
		*/
		public function ver(){
			# 1
			if(isset($_GET["id"])){
				$id = $_GET["id"];
				$producto = new Producto();
				$producto->setId($id);
				$producto = $producto->getOne();
			}
			# 2
			require_once 'views/producto/ver.php';
		}

		/*
			Gestion:
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Instancia un Producto y los obtiene todos
			3. Muestra la Vista 'Producto' (Gestion)
		*/
		public function gestion(){
			# 1
			Utils::isAdmin();
			# 2
			$producto = new Producto();
			$productos = $producto->getAll();
			# 3
			require_once 'views/producto/gestion.php';
		}

		/*
			Crear:
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Muestra la Vista 'Producto' (Crear)
		*/
		public function crear(){
			# 1
			Utils::isAdmin();
			# 2
			require_once 'views/producto/crear.php';
		}

		/*
			Save
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Encapsulamos los Datos del Formulario obtenidos mediante POST
			3. Gestiona la imagen para añadir al Producto
				· Si no está creada la carpeta de subida de imágenes, se crea
				· Movemos el archivo subido a la carpeta anterior
			4. Comprobamos si vamos a Crear o a Editar un Producto
			5. Muestra la Vista 'Producto' (Gestion)
		*/
		public function save(){
			if(isset($_POST)){
				# 1
				Utils::isAdmin();
				# 2
				$categoria_id = isset($_POST["categoria"]) ? (int) $_POST["categoria"] : false;
				$nombre = isset($_POST["nombre"]) ? $_POST["nombre"] : false;
				$descripcion = isset($_POST["descripcion"]) ? $_POST["descripcion"] : false;
				$precio = isset($_POST["precio"]) ? (int) $_POST["precio"] : false;
				$stock = isset($_POST["stock"]) ? (int) $_POST["stock"] : false;
				$imagen = isset($_FILES["imagen"]) ? $_FILES["imagen"] : false;

				if ($categoria_id && $nombre && $descripcion && $precio && $stock && $imagen) {
					$producto = new Producto();
					$producto->setCategoria_id($categoria_id);
					$producto->setNombre($nombre);
					$producto->setDescripcion($descripcion);
					$producto->setPrecio($precio);
					$producto->setStock($stock);

					# 3
					if(isset($_FILES['imagen'])){
						$file = $_FILES["imagen"];
						$filename = $file["name"];
						$mimetype = $file["type"];

						if($mimetype == "image/jpg" || $mimetype == "image/jpeg" || $mimetype == "image/png" || $mimetype == "image/gif"){
							if(!is_dir('uploads/images')){
								mkdir('uploads/images', 0777, true);
							}
							move_uploaded_file($file['tmp_name'], 'uploads/images/'.$filename);
							$producto->setImagen($filename);
						}
					}

					# 4
					if(isset($_GET['id'])){
						$id = $_GET['id'];
						$producto->setId($id);
						$save = $producto->edit();
					} else {
						$save = $producto->save();
					}

					if ($save) {
						$_SESSION['producto'] = "complete";
					} else {
						$_SESSION['producto'] = "failed";
					}
				} else {
					$_SESSION['producto'] = "failed";
				}
			} else {
				$_SESSION['producto'] = "failed";
			}
			# 5
			header("Location:".base_url.'producto/gestion');
		}

		/*
			Editar:
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Instancia un Producto cuyo ID obtiene mediante URL
			3. Muestra la Vista 'Producto' (Crear)
			4. Muestra la Vista 'Producto' (Gestion)
		*/
		public function editar(){
			# 1
			Utils::isAdmin();
			# 2
			if(isset($_GET["id"])){
				$id = $_GET["id"];
				$editar = true;
				$producto = new Producto();
				$producto->setId($id);
				$product = $producto->getOne();
				# 3
				require_once 'views/producto/crear.php';
			} else {
				# 4
				header("Location:".base_url.'producto/gestion');
			}
		}

		/*
			Eliminar
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Instancia un Producto cuyo ID obtiene mediante URL
			3. Muestra la Vista 'Producto' (Gestion)
		*/
		public function eliminar(){
			# 1
			Utils::isAdmin();
			# 2
			if(isset($_GET["id"])){
				$id = $_GET["id"];
				$producto = new Producto();
				$producto->setId($id);
				$delete = $producto->delete();

				if($delete){
					$_SESSION["delete"] = "complete";
				} else {
					$_SESSION["delete"] = "failed";
				}
			} else {
				$_SESSION["delete"] = "failed";
			}
			# 3
			header("Location:".base_url.'producto/gestion');
		}

	}

?>