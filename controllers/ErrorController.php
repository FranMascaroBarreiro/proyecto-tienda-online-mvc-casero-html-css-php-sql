<?php

	class ErrorController {

		/*
			Index:
			1. Muestra un Error 404 en caso de intentar acceder a un Recurso que no existe
				· Lógica aplicada en .htaccess
		*/
		public function index(){
			echo "
				<div id='error_404'>
					<i class='fa-solid fa-circle-exclamation'></i>
					<h1>Error 404</h1>
					<h3>La página que buscas no existe</h3>
				</div>
			";
		}

	}

?>