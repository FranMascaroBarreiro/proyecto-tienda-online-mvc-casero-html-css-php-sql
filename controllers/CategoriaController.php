<?php

	require_once 'models/categoria.php';
	require_once 'models/producto.php';

	class CategoriaController {

		/*
			Index:
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Encapsula en una Variable las distintas Categorías obtenidas mediante Base de Datos
			3. Muestra la Vista 'Categoria' (Listado)
		*/
		public function index(){
			# 1
			Utils::isAdmin();
			# 2
			$categoria = new Categoria();
			$categorias = $categoria->getAll();
			# 3
			require_once 'views/categoria/index.php';
		}

		/*
			Ver:
			1. Encapsulamos el ID de la Categoría que recibimos mediante URL
			2. Encapsula los datos de la Categoría en una Variable que se le pasa a la Vista
			3. Encapsula los datos de los Productos en una Variable que se le pasa a la Vista
			4. Muestra la Vista 'Categoria' (Productos)
		*/
		public function ver(){
			if(isset($_GET['id'])){
				# 1
				$id = $_GET['id'];
				# 2
				$categoria = new Categoria();
				$categoria->setId($id);
				$categoria = $categoria->getOne();
				$categoria = $categoria[0];
				# 3
				$producto = new Producto();
				$producto->setCategoria_id($id);
				$productos = $producto->getAllCategory();
			}
			# 4
			require_once 'views/categoria/ver.php';
		}

		/*
			Crear:
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Muestra la Vista 'Categoria' (Crear)
		*/
		public function crear(){
			# 1
			Utils::isAdmin();
			# 2
			require_once 'views/categoria/crear.php';
		}

		/*
			Save:
			1. Determina si el Uusario loggeado es Admin
				· En caso positivo puede acceder a esta sección, en caso contrario no
			2. Si recibe mediante $_POST el nombre de la Categoría que buscamos crear, la crea
			3. Muestra la Vista 'Categoria' (Listado)
		*/
		public function save(){
			# 1
			Utils::isAdmin();
			# 2
			if(isset($_POST) && isset($_POST['nombre'])){
				$categoria = new Categoria();
				$categoria->setNombre($_POST['nombre']);
				$categoria->save();
			}
			# 3
			header("Location:".base_url."categoria/index");
		}

	}

?>