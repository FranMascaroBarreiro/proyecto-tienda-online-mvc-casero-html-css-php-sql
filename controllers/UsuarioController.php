<?php

	require_once 'models/usuario.php';

	class UsuarioController {

		/*
			1. Muestra Metadata
		*/
		public function index(){
			echo "Controlador Usuarios, Acción index";
		}

		/*
			Registro: Muestra la Vista 'Usuario' (Registro)
		*/
		public function registro(){
			require_once 'views/usuario/registro.php';
		}

		/*
			Save:
			1. Encapsulamos los Datos del Formulario obtenidos mediante POST
			2. Instanciamos un Usuario y lo guardamos en Base de Datos
			3. Muestra la Vista 'Usuario' (Registro)
		*/
		public function save(){
			try {
				if (isset($_POST)) {
					# 1
					$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
					$apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
					$email = isset($_POST['email']) ? $_POST['email'] : false;
					$password = isset($_POST['password']) ? $_POST['password'] : false;
					# 2
					if ($nombre && $apellidos && $email && $password) {
						$usuario = new Usuario();
						$usuario->setNombre($nombre);
						$usuario->setApellidos($apellidos);
						$usuario->setEmail($email);
						$usuario->setPassword($password);
						$save = $usuario->save();
						if ($save) {
							$_SESSION['register'] = "complete";
						} else {
							$_SESSION['register'] = "failed";
						}
					} else {
						$_SESSION['register'] = "failed";
					}
				} else {
					$_SESSION['register'] = "failed";
				}
				# 3
				header("Location:".base_url.'usuario/registro');
			} catch(Exception $e) {
				$code = $e->getCode();
				$file = $e->getFile();
				$message = $e->getMessage();
				$line = $e->getLine();
				$traceString = $e->getTraceAsString();
				echo "<pre>";
				echo "Exception thrown in: <b>$file</b>\n\n";
				echo "on Line: <b>$line</b>\n\n";
				echo "Code: [<b>$code</b>]\n\n";
				echo "with the Message: <b>$message</b>\n\n";
				echo "Trace as string: \n\n <b>$traceString</b>";
				echo "</pre>";
			}
		}

		/*
			Login:
			1. Encapsulamos los Datos del Formulario obtenidos mediante POST
			2. Comparamos los datos del Usuario que intenta acceder con los existentes en Base de Datos
				· Si los datos coinciden, se loguea correctamente
				· Si el Usuario hubiera introducido mal las credenciales generando un Error, éste se elimina
				· Si el Usuario logueado es Admin, se hace saber
			3. Muestra la Página Principal
		*/
		public function login(){
			try {
				if(isset($_POST)){
					# 1
					$usuario = new Usuario();
					$usuario->setEmail($_POST['email']);
					$usuario->setPassword($_POST['password']);
					# 2
					$identity = $usuario->login();
					$identity = $identity[0];
					if ($identity) {
						if($_SESSION['error_login']){
							unset($_SESSION['error_login']);
						}
						$_SESSION['identity'] = $identity;
						if ($identity["rol"] == 'admin') {
							$_SESSION['admin'] = true;
						}
					} else {
						$_SESSION['error_login'] = "Login incorrecto";
					}
				}
				# 3
				header("Location:".base_url);
			} catch(Exception $e) {
				$code = $e->getCode();
				$file = $e->getFile();
				$message = $e->getMessage();
				$line = $e->getLine();
				$traceString = $e->getTraceAsString();
				echo "<pre>";
				echo "Exception thrown in: <b>$file</b>\n\n";
				echo "on Line: <b>$line</b>\n\n";
				echo "Code: [<b>$code</b>]\n\n";
				echo "with the Message: <b>$message</b>\n\n";
				echo "Trace as string: \n\n <b>$traceString</b>";
				echo "</pre>";
			}
		}

		/*
			Logout:
			1. Elimina de la Sesión el Usuario actual logueado
			2. Muestra la Página Principal
		*/
		public function logout(){
			# 1
			if(isset($_SESSION['identity'])){
				unset($_SESSION['identity']);
			}
			if(isset($_SESSION['admin'])){
				unset($_SESSION['admin']);
			}
			# 2
			header("Location:".base_url);
		}
		
	}

?>