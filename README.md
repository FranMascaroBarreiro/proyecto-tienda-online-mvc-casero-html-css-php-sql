# INFO

### Descripción

Este es un proyecto de una Tienda Online en el que se usarán de forma rudimentaria las tecnologías base del desarrollo Back End, usando la arquitectura MVC como base. No se usarán Frameworks para ello, sinó que será construida en PHP nativo

### Finalidad

Desacoplar tecnologías propias de un Framework PHP de cara a buscar, entender, optimizar y personalizar el código necesario para su construcción

### Tecnologías usadas

HTML, CSS, JavaScript, PHP, MySQL

### Paradigma de Programación

POO (Programación Orientada a Objetos)

### Arquitectura

MVC

# DESCRIPCIÓN

### URL

`http://localhost:8888/Masters/masterPHP/110_Tienda_Online/proyecto-tienda-online-mvc-casero-html-css-php-sql/`

### COMPONENTES

`index.php`: Controlador Frontal de nuestra aplicación y puerta de entrada al proyecto de **Tienda Online**

- Carga `autoload.php` para acceder a todas las Clases de nuestra carpeta `/controllers`

- Carga `db.php` que realiza la conexión con la Base de Datos mediante PDO

- Carga `parametrs.php` que se encarga de definer nuestras constantes

- Carga `views/layout` con los distintos componentes de una Vista

- Mediante `show_Error()` se muestra un Error 404 en caso de intentar acceder a un Recurso que no existe

`.htaccess`: Establece una URL que muestra el archivo de **Error 404** en caso de no encontrar el recurso solicitado, así como de transformar las URLs de nuestro proyecto en otras más amigables, de la siguiente manera:

- URL estándar: `/?controller=usuario&action=index`

- URL amigable: `/usuario/index`

`helpers/utils.php`: Librerías útiles para pequeñas funcionalidades

- El método `deleteSession()` se encarga de eliminar la sesión en cuanto se termina de realizar determinada acción

- `Utils::isAdmin()` proprociona un método estático para restringir ciertas acciones de nuestra web al rol de administrador

### LÓGICA

`index.php` contiene la lógica que se encarga de gestionar nuestro MVC casero. Funciona de la siguiente manera:

1. Cargamos el **Controlador**:

	- Comprobamos si llega por la URL y definimos un comportamiento en caso de que esto no suceda

2. Cargamos la **Acción**:

   - Comprobamos si llega por la URL y definimos un comportamiento en caso de que esto no suceda

3. Mostramos un mensaje de aviso en caso de que se produzca algún error

# DETALLES

Para hacer uso de un Usuario que también sea admin, usaremos las siguientes credenciales:

- Admin

	- Correo: `admin@admin.com`
	- Contraseña: `abc123..`
	- Enlaces: **Carrito**, **Gestionar categorías**, **Gestionar productos**, **Gestionar pedidos**, **Mis pedidos**, **Cerrar Sesión**

- Usuario

	- Correo: `check@check.es`
	- Contraseña: `check`
	- Enlaces: **Carrito**, **Mis pedidos**, **Cerrar Sesión**

# DEBUG

Sin resalte de sintaxis

```php
	echo "<pre>";
	echo "<b>\$productos</b>\n\n";
	var_dump($productos);
	echo "</pre>";
```

Con resalte de sintaxis

```php
	echo "<pre>";
	highlight_string("<?php\n\n".var_export($productos, true)."\n\n?>");
	echo "</pre>";
```

# RECURSOS

`assets/resources`: Recursos de Diseño Gráfico 

- 1 - Short Sleeve Shirts - Camisas de manga corta
- 2 - Long Sleeve Shirts - Camisas de manga larga
- 3 - Hoodies - Sudaderas con capucha
- 4 - Sweatpants - Pantalones de chándal
- 5 - Sneakers - Zapatillas de deporte

`assets/img`: Imágenes optimizadas de los Recursos anteriores