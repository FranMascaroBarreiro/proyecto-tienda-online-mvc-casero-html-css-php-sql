/*
	masterPHP_Tienda_Online
*/

use masterPHP_Tienda_Online;

/*
	Tablas
*/

select * from categorias;
select * from lineas_pedidos;
select * from pedidos;
select * from productos;
select * from usuarios;

/*
	Usuarios
*/

select * from usuarios;

# Actualizamos los Usuarios actuales

update usuarios
set nombre = "Fran",
apellidos = "Mascaró Barreiro",
password = "$2y$04$YZoklUU06uAmeWASRv/SkOqUYhsZqVgXXMvo01J0HmGIqz4.suYNa"
where id = 1;

update usuarios
set nombre = "Algo",
apellidos = "Otro"
where id = 2;

update usuarios
set nombre = "Admin",
apellidos = "Admin",
password = "$2y$04$YZoklUU06uAmeWASRv/SkOqUYhsZqVgXXMvo01J0HmGIqz4.suYNa"
where id = 10;

/*
	Pedidos
*/

# Asignamos los pedidos existentes a mi usuario actualizado

select * from usuarios;
select * from usuarios where id = 1; # YO

select * from pedidos;
update pedidos set usuario_id = 1;

/*
	Productos
*/

# Productos

select * from productos;

# Actualizamos la Categoria de una camiseta de manga larga

update productos set categoria_id = 2 where id = 17;

# Actualizamos / Insertamos productos

# 1 - Short Sleeve Shirts

select * from productos where categoria_id = 1;

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(1, "Short Sleeve Shirt", "Yeezy Gap Engineered by Balenciaga", 220, 10, now(), "Short-Sleeve-Shirt-3.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(1, "Short Sleeve Shirt", "Yeezy Gap Engineered by Balenciaga", 250, 10, now(), "Short-Sleeve-Shirt-4.png");

update productos
set descripcion = "Palm Angels",
fecha = now(),
imagen = "Short-Sleeve-Shirt-5.png"
where id = 6;

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(1, "Short Sleeve Shirt", "Palm Angels", 230, 3, now(), "Short-Sleeve-Shirt-6.png");

# 2 - Long Sleeve Shirts

select * from productos where categoria_id = 2;

update productos
set precio = 235,
stock = 12,
imagen = "Long-Sleeve-shirt-3.png"
where id = 7;

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(2, "Long Sleeve Shirts", "Palm Angels", 255, 13, now(), "Long-Sleeve-shirt-4.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(2, "Long Sleeve Shirts", "Palm Angels", 225, 15, now(), "Long-Sleeve-shirt-5.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(2, "Long Sleeve Shirts", "Yeezy Gap Engineered by Balenciaga", 225, 15, now(), "Long-Sleeve-shirt-6.png");

# 3 - Hoodies

select * from productos where categoria_id = 3;

update productos
set descripcion = "Yeezy Gap Engineered by Balenciaga",
precio = 280,
stock = 7
where id = 3;

update productos
set descripcion = "Yeezy Gap Engineered by Balenciaga",
imagen = "Hoodie-2.png",
precio = 275,
stock = 12
where id = 8;

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(3, "Hoodie", "Palm Angels", 445, 3, now(), "Hoodie-3.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(3, "Hoodie", "Palm Angels", 475, 7, now(), "Hoodie-4.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(3, "Hoodie", "Palm Angels", 415, 7, now(), "Hoodie-5.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(3, "Hoodie", "Palm Angels", 520, 7, now(), "Hoodie-6.png");

# 4 - Sweatpants

select * from productos where categoria_id = 4;

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(4, "Sweatpant", "Fuck The Population", 175, 17, now(), "Sweatpant-2.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(4, "Sweatpant", "Vlone", 245, 9, now(), "Sweatpant-3.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(4, "Sweatpant", "Vlone", 215, 13, now(), "Sweatpant-5.png");

# 5 - Sneakers

select * from productos where categoria_id = 5;

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(5, "Sneakers", "Yeezy Foam RNR MX Carbon", 215, 3, now(), "sneakers-3.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(5, "Sneakers", "Yeezy 450 Cloud White", 325, 3, now(), "sneakers-4.png");

insert into productos (categoria_id, nombre, descripcion, precio, stock, fecha, imagen) values
(5, "Sneakers", "Yeezy Boost 700 Wash", 475, 3, now(), "sneakers-5.png");

# Categoria

select * from categorias;

/*
	MVC
*/

# Productos

select * from productos;
select * from productos where id = 3;
select * from productos where categoria_id = 1;

SELECT p.*, c.nombre as 'catnombre'
FROM productos p
INNER JOIN categorias c ON c.id = p.categoria_id
WHERE p.categoria_id = 1
ORDER BY id DESC;

SELECT p.*, lp.unidades FROM productos p
INNER JOIN lineas_pedidos lp ON p.id = lp.producto_id
WHERE pedido_id = 27;

SELECT * FROM productos ORDER BY RAND() LIMIT 6;

# Usuarios

select * from usuarios;
select * from usuarios where id = 1;

# Categorías

select * from categorias;
select * from categorias where id = 1;

delete from categorias where id in (6, 7, 8, 9, 10, 11);

# Pedidos

select * from pedidos;
select * from pedidos order by id desc;

SELECT * FROM pedidos p
INNER JOIN lineas_pedidos lp ON lp.pedido_id = p.id
WHERE usuario_id = 1
ORDER BY p.id DESC
LIMIT 1;

# Lineas pedidos

select * from lineas_pedidos;
select * from lineas_pedidos where pedido_id = 27;